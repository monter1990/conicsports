<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Applications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ps_camp_applications', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('id_user')->nullable()->default(NULL);
        $table->integer('location')->nullable()->default(NULL);
        $table->integer('pupil')->nullable()->default(NULL);
        $table->integer('sport_category')->nullable()->default(NULL);
        $table->integer('level')->nullable()->default(NULL);
        $table->integer('time_frame')->nullable()->default(NULL);
        $table->integer('week_day')->nullable()->default(NULL);
        $table->timestamps();
    });
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('ps_camp_applications');
    }
}

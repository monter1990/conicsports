<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default("");
            $table->integer('id_gender')->default(0);
            $table->string('email')->unique()->default("");
            $table->string('password')->default("");
            $table->string('first_name')->default("");
            $table->string('last_name')->default("");
            $table->date('birthday')->nullable()->default(NULL);
            $table->string('phone_number')->default("");
            $table->string('country')->default("");
            $table->string('street')->default("");
            $table->string('number')->default("");
            $table->string('zip_code')->default("");
            $table->string('town')->default("");
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePupilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pupils', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->nullable()->default(NULL);
            $table->integer('id_gender')->nullable()->default(NULL);
            $table->string('first_name')->default('');
            $table->string('last_name')->default('');
            $table->date('birthday')->nullable()->default(NULL);
            $table->string('sport')->nullable()->default(NULL);
            $table->string('level')->nullable()->default(NULL);
            $table->string('weight')->nullable()->default(NULL);
            $table->string('height')->nullable()->default(NULL);
            $table->string('foot_size')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pupils');
    }
}

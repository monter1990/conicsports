<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->call(LevelsTableSeeder::class);
      $this->command->info('levels table seeded!');

      $this->call(AgeCategoryTableSeeder::class);
      $this->command->info('age_category table seeded!');
   }

}

class LevelsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
      DB::table('levels')->delete();
      DB::table('levels')->insert([
        [ 'level_name' => 'Beginner' ],
        [ 'level_name' => 'Leisure 1' ],
        [ 'level_name' => 'Leisure 2' ],
        [ 'level_name' => 'Promissing' ],
        [ 'level_name' => 'Advance' ]
      ]);
    }
  }
  class AgeCategoryTableSeeder extends Seeder
  {
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
      DB::table('age_category')->delete();
      DB::table('age_category')->insert([
        [ 'category' => 'Beginner', 'age' => '4-5' ],
        [ 'category' => 'SuperMini', 'age' => '6-7' ],
        [ 'category' => 'Mini', 'age' => '8-9' ],
        [ 'category' => 'Poussin', 'age' => '10-11' ],
        [ 'category' => 'Benjamin', 'age' => '12-13' ],
        [ 'category' => 'Minime', 'age' => '14-15' ],
        [ 'category' => 'Cadet', 'age' => '16-17'],
        [ 'category' => 'Junior', 'age' => '18-21' ],
        [ 'category' => 'Senior', 'age' => '21-35' ],
        [ 'category' => 'Master 1', 'age' => '40' ],
        [  'category' => 'Master 2', 'age' => '50' ],
        [ 'category' => 'Master 3', 'age' => '60' ]
      ]);
    }
  }
  class LocationsTableSeeder extends Seeder
  {
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
      DB::table('locations')->delete();
      DB::table('locations')->insert([
        [ 'name_location' => 'EBENE SSS GIRL', 'confirmed' => '1', 'color' => 'blue' ],
        [ 'name_location' => 'GRAND BAIE LA CROISETTE', 'confirmed' => '1', 'color' => 'green' ],
        [ 'name_location' => 'FLACQ COEUR DE VILLE', 'confirmed' => '1', 'color' => 'red' ],
        [ 'name_location' => 'SSR WINTER HOLIDAYS CAMP', 'confirmed' => '1', 'color' => 'pink' ],
        [ 'name_location' => 'MOKA', 'confirmed' => '0', 'color' => 'white' ],
        [ 'name_location' => 'CUREPIPE', 'confirmed' => '0', 'color' => 'black' ],
        [ 'name_location' => 'CASCAVELLE', 'confirmed' => '0', 'color' => 'yellow' ],
        [ 'name_location' => 'TAMARIN', 'confirmed' => '0', 'color' => 'purple' ],
        [ 'name_location' => 'ALBION', 'confirmed' => '0', 'color' => 'teal' ],
        [ 'name_location' => 'RICHE TERRE', 'confirmed' => '0', 'color' => 'gray' ],
        [ 'name_location' => 'RIVIERE DU REMPART', 'confirmed' => '0', 'color' => 'orange' ],
        [ 'name_location' => 'ROSE BELLE', 'confirmed' => '0', 'color' => 'indigo' ],
        [ 'name_location' => 'CHEMIN GRENIER', 'confirmed' => '0', 'color' => 'cyan' ]
      ]);
    }
  }
class SportsCategoryTableSeeder extends Seeder
{
  /**
   * Auto generated seed file.
   *
   * @return void
   */
  public function run()
  {
    DB::table('sports_category')->delete();
    DB::table('sports_category')->insert([
      [ 'category_name' => 'Inline Skating', 'group_age_id' => '1', 'icon' => 'inline_skating.png' ],
      [ 'category_name' => 'Crossminton', 'group_age_id' => '1', 'icon' => 'crossminton1.png' ],
      [ 'category_name' => 'Skateboarding', 'group_age_id' => '1', 'icon' => 'skateboarding.png' ],
      [ 'category_name' => 'Crousing', 'group_age_id' => '1', 'icon' => 'crousing.png' ]
    ]);
  }
}
class ContactsTableSeeder extends Seeder
{
  /**
   * Auto generated seed file.
   *
   * @return void
   */
  public function run()
  {
    DB::table('contacts')->delete();
    DB::table('contacts')->insert(
      [ 'phone' => 'enter_your_phone', 'mobile1' => 'enter_your_mobile1', 'mobile2' => 'enter_your_mobile1', 'fax' => 'enter_your_fax', 'email' => 'enter_your_email', 'adress' => 'enter_your_adress' ]
    );
  }
}
class TimeframesTableSeeder extends Seeder
{
  /**
   * Auto generated seed file.
   *
   * @return void
   */
  public function run()
  {
    DB::table('timeframes')->delete();
    DB::table('timeframes')->insert([
      [ 'time_frame' => '08:00 - 09:00' ],
      [ 'time_frame' => '09:15 - 10:15' ],
      [ 'time_frame' => '10:30 - 11:30' ],
      [ 'time_frame' => '11:45 - 12:45' ],
      [ 'time_frame' => '13:00 - 14:00' ],
      [ 'time_frame' => '14:15 - 15:15' ],
      [ 'time_frame' => '15:45 - 16:45' ]
    ]);
  }
}

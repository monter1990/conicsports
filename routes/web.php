<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
Route::post('/information', 'InformationController@change_info')->name('home');
Route::post('/trainees_ins', 'PupilsController@insert_info_p')->name('pupil_ins');
Route::post('/trainees_upd', 'PupilsController@change_info_p')->name('pupil_upd');
Route::post('/trainees_sel', 'PupilsController@index')->name('selected_pupil_view');
Route::post('/new_reservations', 'ReservationsController@register_pupil')->name('registration');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/information', 'InformationController@index')->name('information');
Route::get('/trainees', 'PupilsController@index')->name('pupils');
Route::get('/trainees?new', 'PupilsController@index')->name('new_trainee');
Route::get('/reservations?location=', 'ReservationsController@index')->name('new_reservation');
Route::get('/reservations', 'ReservationsController@index')->name('reservations');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

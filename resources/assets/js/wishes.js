/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var daysofweek = {1:'Monday',2:'Tuesday',3:'Wednesday',4:'Thursday',5:'Friday',6:'Saturday',7:'Sunday'};
var currentRequest = null;
var location_name;
var location_id;
var popover_id;
var is_filtered = false;
var checked_filter = [];
var group_age_box = [];
var levels_box = [];
var count_value = 0;
var pupils = [];
var day;
var location;
var sport;
var time;

$( document ).ready(function() {
    var image = { width: $('#mapimage').width(), height: $('#mapimage').height() };
    var target = new Array();
    target[0] = { x: image.width/2.8+20, y: image.height/1.85+20 };
    target[1] = { x: image.width/1.85+20, y: image.height/14.3+20 };
    target[2] = { x: image.width/1.2+20, y: image.height/2.9+20 };
    target[3] = { x: image.width/1.7+20, y: image.height/2+20 };
    target[4] = { x: image.width/2.1+20, y: image.height/2.15+20 };
    target[5] = { x: image.width/2.35+20, y: image.height/1.70+20 };
    target[6] = { x: image.width/5+20, y: image.height/1.9+20 };
    target[7] = { x: image.width/6.7+20, y: image.height/1.49+20 };
    target[8] = { x: image.width/4.65+20, y: image.height/2.65+20 };
    target[9] = { x: image.width/2.3+20, y: image.height/3.9+20 };
    target[10] = { x: image.width/1.25+20, y: image.height/4.5+20 };
    target[11] = { x: image.width/1.65+20, y: image.height/1.33+20 };
    target[12] = { x: image.width/3.15+20, y: image.height/1.095+20 };
    var pointer = new Array();
    pointer[0] = $('.pointer.1');
    pointer[1] = $('.pointer.2');
    pointer[2] = $('.pointer.3');
    pointer[3] = $('.pointer.4');
    pointer[4] = $('.pointer.5');
    pointer[5] = $('.pointer.6');
    pointer[6] = $('.pointer.7');
    pointer[7] = $('.pointer.8');
    pointer[8] = $('.pointer.9');
    pointer[9] = $('.pointer.10');
    pointer[10] = $('.pointer.11');
    pointer[11] = $('.pointer.12');
    pointer[12] = $('.pointer.13');
    getSize(target, pointer, image);
    updatePointer(target, pointer_wrap, image);
});

function getSize(target, pointer, image){

    $(document).ready(updatePointer(target, pointer, image));
    $(window).resize(function(){
        updatePointer(target, pointer, image);
    });
}
function updatePointer(target, pointer, image) {
    var windowWidth = $('#mapimage').width();
    var windowHeight = $('#mapimage').height();
    
    // Get largest dimension increase
    var xScale = windowWidth / image.width;
    var yScale = windowHeight / image.height;
    var scale;
    var yOffset = 0;
    var xOffset = 0;
    
    if (xScale > yScale) {
        // The image fits perfectly in x axis, stretched in y
        scale = xScale;
        yOffset = (windowHeight - (image.height * scale)) / 2;
        console.log(yOffset);
    } else {
        // The image fits perfectly in y axis, stretched in x
        scale = yScale;
        xOffset = (windowWidth - (image.width * scale)) / 2;
        console.log(xOffset);
    }
    
    var arrayLength = target.length;

    for (var i = 0; i < arrayLength; i++) {
        pointer[i].css('top', (target[i].y) * scale + yOffset);
        pointer[i].css('left', (target[i].x) * scale + xOffset);
        pointer[i].css('display', 'block');
        
    }
}
$(document).on('change', ".sport_all input", function(event){
    if($(this).prop('checked')){
        $('.sport input').each(function(){
            $(this).prop('checked', true);
            $(this).parents().eq(1).addClass('checked');
            $(this).parents().eq(1).css('background', 'green');
        });
    }else{
        $('.sport input').each(function(){
            $(this).prop('checked', false);
            $(this).parents().eq(1).removeClass('checked');
            $(this).parents().eq(1).css('background', 'transparent');
        });
    }
});
$(document).on('hidden.bs.modal', '#modal', function () {
    $('#modal').modal('dispose');
});
$('.plus_plus').hover(function(){
    $('i', this).css('display', 'block');
    $(this).css('cursor', 'pointer');
    $('i', this).append('Apply here.');
   },
   function(event){
    $('i', this).css('display', 'none');
    $('i', this).html('');
   });

$('.pointer').hover(function(){
    $(this).children().css('display', 'block');
    if($(this).hasClass('11') || $(this).hasClass('3')){
        $(this).children().css('margin-left', '-80px');
        $(this).children().css('margin-top', '5px');
    }
},
function(event){
    $(this).children().css('display', 'none');
});

$('.plus_plus').click(function(){
    location = '';
    sport = '';
    day = '';
    time = '';
    console.log($(this).data('day'));
    day = $(this).data('day');
    $('#sport').html('Sport: '+$('.modal-nav-item.active a').data('name'));
    $("input[name='sport']").val($('.modal-nav-item.active a').attr('id'));
    $('#day').attr('data-id', $(this).data('day'));
    $("input[name='day']").val($(this).data('day'));
    $('#time').attr('data-id', $(this).data('timeid'));
    $("input[name='time']").val($(this).data('timeid'));
    $('#day').html('Day: '+daysofweek[day]);
    $('#time').html('Time: '+$(this).data('time'));
    var registered_users = $(this).data('registered').trim();
    registered_users = registered_users.replace(" ", "");
    registered_users = registered_users.substring(0, registered_users.length - 1);
    console.log('registered_users= '+registered_users);
    var registered_users_array = registered_users.split(',');
    $("input[type='checkbox']").each(function(){
        $(this).prop('checked', false);
        $(this).parents().eq(1).css('background', 'transparent');
    });
    location = $("b[name='location']").attr('id');
    sport = $('.modal-nav-item.active a').attr('id');
    day = $(this).data('day');
    time =  $(this).data('timeid');
    $('#modal').modal({
        cashe:false,
    });    
    if(registered_users.length!=0){
        registered_users_array.forEach(function(index, element){
            console.log('index= '+index);
            index = index.trim();
            console.log('#applied_'+element+'<br>');
            console.log($('#applied_'+element));
            $('#applied_'+index).prop('checked', true);
            $('#applied_'+index).parents().eq(1).css('background', 'green');
        });
    }
});
/*$('.register').click(function(){
    pupils = [];
    console.log('register');
    var checked_pupils = $('.sport input:checked');
    console.log(checked_pupils);
    checked_pupils.each(function() {
        var id_pupil = $(this).attr('name').replace('applied', '');
        pupils.push(id_pupil);
        console.log(pupils);
    });
    registe_request('http://localhost/conic/public/register_trainee', {'location': location, 'sport':sport, 'pupils':pupils, 'day':day, 'time':time});
    
});*/
function registe_request($url, $data){
    $('.loading').css('display', 'block');
    $.ajax({
        type:'POST',
        url:$url,
        data:$data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(data){
            console.log(data);
//            $('.flash-message').html(data);
            $('.flash-message').html("<div class='alert alert-success alert-block'><button type='button' class='close' data-dismiss='alert'>×</button><strong>New reservation was successfully made.</strong></div>");
            if(data.length>0){
                $('.flash-message').append("<div class='alert alert-warning'><button type='button' class='close' data-dismiss='alert'>×</button><strong>Following regestrations were deleted:<ul class='del_regis'></ul></strong></div>");
                for(var i=0;i<data.length;i++){
                    $('.flash-message .alert-warning strong ul').append('<li>'+(i+1)+'. '+data[i]+'</li>');
                }
            }
            $('.loading').css('display', 'none');
        },
        error: function(xhr, textStatus, error){
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
            $('.flash-message').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>×</button><strong>Somethins went wrong. Please, reload the page and try again.</strong></div>");
            $('.loading').css('display', 'none');
        }
    });
   
}

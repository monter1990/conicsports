
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./trainees');
require('./wishes');
window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});
$( document ).ready(function() {
    console.log( "ready!" );
    Tabs.init();
});
var Tabs = {

    init: function() {
      this.bindUIfunctions();
      this.pageLoadCorrectTab();
    },
  
    bindUIfunctions: function() {
  
      // Delegation
      $(document)
        .on("click", ".transformer-tabs .link-list a:not('.active')", function(event) {
          Tabs.changeTab(this.id);
          event.preventDefault();
        })
        .on("click", ".transformer-tabs .link-list a.active", function(event) {
          Tabs.toggleMobileMenu(event, this);
          event.preventDefault();
        });
  
    },
  
    changeTab: function(hash) {
  
      var anchor = $('.transformer-tabs .link-list a[id="' + hash + '"]');
      // activate correct anchor (visually)
      anchor.addClass("active").parent().siblings().find("a").removeClass("active");
      anchor.parent().parent().find("li").removeClass("active"); 
      anchor.parent().addClass("active").css('overflow', 'visible');
      anchor.parent().siblings(":not(.active)").css('overflow', 'hidden');
  //    $('.link-list li:not(:first-child)').css('margin-top', '0px');
  // update URL, no history addition
      // You'd have this active in a real situation, but it causes issues in an <iframe> (like here on CodePen) in Firefox. So commenting out.
      // window.history.replaceState("", "", hash);
  
      // Close menu, in case mobile
  //    AreqX(ajax_link, {action:'pupilspage'});        
      AreqX(hash+"?content_only=1");        
      anchor.closest("ul").removeClass("open");
  
    },
  
    // If the page has a hash on load, go to that tab
    pageLoadCorrectTab: function() {
      if($('body').hasClass('page-my-account')){
          console.log($('.link-list:first-child a'));
          if(isAjaxing){
              return;
          }
          if(!($('.link-list li:nth-child(n) a').hasClass("active"))){
              isAjaxing = true;
              $('.link-list li:first-child a').addClass("active");
  $('.link-list li:first-child').addClass("active");
      
              AreqX($('.link-list li .active').attr('id')+"?content_only=1", );        
          }
      }
    },
  
    toggleMobileMenu: function(event, el) {
  //$('.link-list .nav-link.active').css('margin-top', '5px');
  //$('.link-list .nav-link.active').css('margin-bottom', '5px');
      $(el).closest("ul").toggleClass("open");
    }
  
  }
  
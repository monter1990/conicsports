$( document ).ready(function() {
//    $("#"+$("input[name='id']").val()).addClass('active'); 
});
function Ajax_request($url, $data){
//    console.log('ajax');
//    console.log($url);
//    console.log($data);

    $.ajax({
        type:'POST',
        url:$url,
        data:$data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(data){
            console.log(data);
            $("input[name='id']").val(data.selected_pupil.id);
            if(data.selected_pupil.id_gender==0){
                $("#Male").prop('checked', true);
            }else{
                $("#Female").prop('checked', true);
            }
            $("input[name='first_name']").val(data.selected_pupil.first_name);
            $("input[name='last_name']").val(data.selected_pupil.last_name);
            $("select[name='days']").val(data.selected_pupil.birthday.split('-')[2]);
            $("select[name='months']").val(data.selected_pupil.birthday.split('-')[1]);
            $("select[name='years']").val(data.selected_pupil.birthday.split('-')[0]);
            if(data.selected_pupil.sport.split(', ')[0]==1){
                $("input[name='inline_skating']").prop('checked', true);
                $("input[name='inline_skating']").parents().eq(1).css('background', 'green');
            }else{
                $("input[name='inline_skating']").prop('checked', false);
                $("input[name='inline_skating']").parents().eq(1).css('background', 'transparent');
            }
            if(data.selected_pupil.sport.split(', ')[1]==1){
                $("input[name='crossminton']").prop('checked', true);
                $("input[name='crossminton']").parents().eq(1).css('background', 'green');
            }else{
                $("input[name='crossminton']").prop('checked', false);
                $("input[name='crossminton']").parents().eq(1).css('background', 'transparent');
            }
            if(data.selected_pupil.sport.split(', ')[2]==1){
                $("input[name='skateboarding']").prop('checked', true);
                $("input[name='skateboarding']").parents().eq(1).css('background', 'green');
            }else{
                $("input[name='skateboarding']").prop('checked', false);
                $("input[name='skateboarding']").parents().eq(1).css('background', 'transparent');
            }
            if(data.selected_pupil.sport.split(', ')[3]==1){
                $("input[name='crousing']").prop('checked', true);
                $("input[name='crousing']").parents().eq(1).css('background', 'green');
            }else{
                $("input[name='crousing']").prop('checked', false);
                $("input[name='crousing']").parents().eq(1).css('background', 'transparent');
            }
            if(data.selected_pupil.level.split(', ')[0]!=0){
                $("select[name='inline_skating_level']").val(data.selected_pupil.level.split(', ')[0]);
                $("select[name='inline_skating_level']").prop('disabled', false);
            }else{
                $("select[name='inline_skating_level']").val('');
                $("select[name='inline_skating_level']").prop('disabled', true);
            }
            if(data.selected_pupil.level.split(', ')[1]!=0){
                $("select[name='crossminton_level']").val(data.selected_pupil.level.split(', ')[1]);
                $("select[name='crossminton_level']").prop('disabled', false);
            }else{
                $("select[name='crossminton_level']").val('');
                $("select[name='crossminton_level']").prop('disabled', true);
            }
            if(data.selected_pupil.level.split(', ')[2]!=0){
                $("select[name='skateboarding_level']").val(data.selected_pupil.level.split(', ')[2]);
                $("select[name='skateboarding_level']").prop('disabled', false);
            }else{
                $("select[name='skateboarding_level']").val('');
                $("select[name='skateboarding_level']").prop('disabled', true);
            }
            if(data.selected_pupil.level.split(', ')[3]!=0){
                $("select[name='crousing_level']").val(data.selected_pupil.level.split(', ')[3]);
                $("select[name='crousing_level']").prop('disabled', false);
            }else{
                $("select[name='crousing_level']").val('');
                $("select[name='crousing_level']").prop('disabled', true);
            }
            $("input[name='weight']").val(data.selected_pupil.weight);
            $("input[name='height']").val(data.selected_pupil.height);
            $("input[name='foot_size']").val(data.selected_pupil.foot_size);
        },
        done:function(data){
            console.log('done');
        }
    });
}
   
$(document).on('click', ".pupilsli span:not('.active')", function(event){
//    event.preventDefault();
    $(this).parent().parent().find('span.active').removeClass('active');
    $(this).addClass('active');
    Ajax_request('http://localhost/conic/public/trainees_sel', {'sel_id':$(this).attr('id')} );   
});

$(document).on('click', ".trashbutton", function(event){
    console.log($('.pupilsul').find('.active').length>0);
$('#modal_delete').modal({
    cashe:false,
    });
   $('.modal-header').html('Deleting trainee '+$(".pupilsli span.active").text().split(' ')[1]+' '+$(".pupilsli span.active").text().split(' ')[2]+'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
});
$(document).on('click', "#modal_delete .btn-danger", function(event){
    if($('.pupilsul').find('.active').length>0){
        console.log('Trash');
        event.preventDefault();
        console.log('length '+$('.pupilsul').children().length);
        var activespanid = $(".pupilsli span.active").attr('id');
            if($('.pupilsli span.active').parent().prev().length>0){
                var prevactiveid = $('.pupilsli span.active').parent().prev().find('span').attr('id');
                $('.pupilsli span.active').parent().prev().addClass('active');
            }else{
                var prevactiveid = $('.pupilsli span.active').parent().next().find('span').attr('id');
                $('.pupilsli span.active').parent().next().addClass('active');
            }
        if($('.pupilsul').children().length>0){
            console.log('getpupil prevactiveid = '+prevactiveid);
            AreqX($("#customer-form").attr('action'), {action:"removepupil",pupil:prevactiveid,id:activespanid});   
//            AreqX(ajax_link, {action:"removepupil",id:activespanid, prevactive:prevactiveid}, 'getpupil');
        }else{
            console.log('emptylist');
            AreqX($("#customer-form").attr('action'), {action:"removepupil",id:activespanid});   
//            AreqX(ajax_link, {action:"removepupil",id:activespanid, emptylist:true}, 'addpupil');
        }
        $('.pupilsli span.active').remove();
    }
    $(this).blur();
    $('#modal_delete').modal('hide');
});

$(document).on('change', ".sport input", function(event){
    console.log($(this).prop('checked'));
    var thisid = $(this).attr('name')+'_level';
    if($(this).prop('checked')){
        if($(this).parents().eq(1).hasClass('regist')){
            $(this).parents().eq(1).addClass('checked');
            console.log('regist');
        }
        $(this).parents().eq(1).css('background', 'green');
        console.log($(this).prop('checked'));
        $('#'+thisid).prop('disabled', false);
    }else{
        if($(this).parents().eq(1).hasClass('regist')){
            $(this).parents().eq(1).removeClass('checked');
            console.log('regist');
        }
        $(this).parents().eq(1).css('background', 'transparent');
        $('#'+thisid).prop('disabled', true);
        $('#'+thisid+' option[value=""]').prop('selected', true);
    }
});
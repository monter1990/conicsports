@extends('layouts.app')

@section('content')
<div class="container auth-container">
        <nav class="transformer-tabs d-md-flex flex-md-row" id="" role='navigation'>
            {!! $MyNavBar->asUl(['class' => 'link-list', 'id' => 'myTab', 'role' => 'tablist']) !!}
                    <div class="tab-content py-3 py-md-0">
                            <div class="loading">
                                    <div class="loading-wheel"></div>
                            </div>
                            <h1 class="conic-header">Your personal information</h1>
                            @if (isset($partials))
                            {!! $partials !!}
                            @endif
</div>
</nav>
            <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
</div>
@endsection

<aside id="notifications">
        <article class="notification notification-danger" role="alert" data-alert="danger">
          <ul>
                <li>Could not update your information, please check your data.</li>
          </ul>
        </article>
</aside>
    <form class="form-horizontal" method="POST" action="{{ route($route) }}">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{old('id', isset($selected_pupil)?$selected_pupil->id:'')}}">
        
        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">Gender</label>
            <div class="col-md-6">
                <label class="radio" for="Male">Male
                    <input class="form-check-input" name="id_gender" type="radio" id="Male" value="0" required="" {{ (isset($selected_pupil)?$selected_pupil->id_gender:old('id_gender'))==0?'checked':''}}>  <i class="checkround"></i>
                </label>

                <label class="radio" for="Female">Female
                    <input class="form-check-input" name="id_gender" type="radio" id="Female" value="1" required="" {{ (isset($selected_pupil)?$selected_pupil->id_gender:old('id_gender'))==1?'checked':''}}>  <i class="checkround"></i>
                </label>
                @if ($errors->has('gender'))
                        <span class="help-block">
                        <strong>{{ $errors->first('gender') }}</strong>
                        </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">First Name</label>

                <div class="col-md-6">
                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name', isset($selected_pupil)?$selected_pupil->first_name:'') }}" required autofocus>

                @if ($errors->has('first_name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                @endif
                </div>
        </div>
        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">Last Name</label>

                <div class="col-md-6">
                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name', isset($selected_pupil)?$selected_pupil->last_name:'') }}" required autofocus>

                @if ($errors->has('last_name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                @endif
                </div>
        </div>
        <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">Birthday</label>
                <div class="field">
                        <div class="col-xs-4 days">
                                <select id="days" name="days" value="{{ old('days') }}" class="form-control" required="">
                                        <option value="" disabled="" selected hidden="">Day</option>
                                                @foreach($select_values['days'] as $index => $day)
                                                <option value={{$index}} {{(old('days', isset($selected_pupil)?explode('-', $selected_pupil->birthday)[2]:'') == $index?'selected':'')}} >{{$day}}</option>
                                                @endforeach
                                </select>
                        </div>
                                                
                        <div class="col-xs-4 months">
                                <select id="months" name="months" class="form-control"  required="">
                                        <option value="" disabled="" selected hidden="">Month</option>
                                                @foreach($select_values['months'] as $index => $month)
                                                <option value={{$index}} {{old('months', isset($selected_pupil)?explode('-', $selected_pupil->birthday)[1]:'') == $index?'selected':''}} >{{$month}}</option>
                                                @endforeach
                                </select>
                        </div>
                        <div class="col-xs-4 years">
                                <select id="years" name="years" class="form-control"  selected="{{ old('years') }}" required="">
                                        <option value="" disabled="" selected hidden="">Year</option>
                                                @foreach($select_values['years'] as $index => $year)
                                                <option value={{$index}} {{old('years', isset($selected_pupil)?explode('-', $selected_pupil->birthday)[0]:'') == $index?'selected':''}} >{{$year}}</option>
                                                @endforeach
                                </select>
                        </div>
                </div>
        </div>    
        <div class="form-group{{ $errors->has('inline_skating') ||  $errors->has('crossminton') ||  $errors->has('skateboarding') ||  $errors->has('crousing') ? ' has-error' : '' }}">
            @if ($errors->has('inline_skating') ||  $errors->has('crossminton') ||  $errors->has('skateboarding') ||  $errors->has('crousing') )
                    <span class="help-block">
                    <strong>{{ $errors->first('inline_skating') }}</strong>
                    </span>
            @endif
            <label class="col-md-4 control-label">Sport</label>
            <div class="row" style="display:inline-block; float: left; width:50%">
                <div class="col-md-6">
                    <div class="checkbox" style="background: {{ (isset($selected_pupil)?explode(', ', $selected_pupil->sport)[0]:'') == 1  || old('inline_skating') == 'on' ? 'green' : 'transparent' }} ">
                    <label class="sport" for="inline_skating">
                            <input type="checkbox" id="inline_skating" name="inline_skating" {{ old('inline_skating', isset($selected_pupil)?explode(', ', $selected_pupil->sport)[0]:'') == 1  || old('inline_skating') == 'on' ? 'checked' : '' }} > Inline Skating
                        <i class="fas fa-check"></i>
                    </label>
                    </div>
                    <div class="checkbox" style="background: {{ (isset($selected_pupil)?explode(', ', $selected_pupil->sport)[1]:'') == 1  || old('crossminton') == 'on' ? 'green' : 'transparent' }} ">
                        <label class="sport" for="crossminton">
                            <input type="checkbox" id="crossminton" name="crossminton" {{ old('crossminton', isset($selected_pupil)?explode(', ', $selected_pupil->sport)[1]:'') == 1  || old('crossminton') == 'on' ? 'checked' : '' }}> Crossminton
                            <i class="fas fa-check"></i>
                        </label>
                    </div>
                    <div class="checkbox" style="background: {{ (isset($selected_pupil)?explode(', ', $selected_pupil->sport)[2]:'') == 1 || old('skateboarding') == 'on' ? 'green' : 'transparent' }} ">
                            <label class="sport" for="skateboarding">
                                <input type="checkbox" id="skateboarding" name="skateboarding" {{ old('skateboarding', isset($selected_pupil)?explode(', ', $selected_pupil->sport)[2]:'') == 1  || old('skateboarding') == 'on' ? 'checked' : '' }}> Skateboarding
                                <i class="fas fa-check"></i>
                            </label>
                    </div>
                    <div class="checkbox" style="background: {{ (isset($selected_pupil)?explode(', ', $selected_pupil->sport)[3]:'') == 1 || old('crousing') == 'on' ? 'green' : 'transparent' }} ">
                            <label class="sport" for="crousing">
                                <input type="checkbox" id="crousing" name="crousing" {{ old('crousing', isset($selected_pupil)?explode(', ', $selected_pupil->sport)[3]:'') == 1 || old('crousing') == 'on' ? 'checked' : '' }}> Crousing
                                <i class="fas fa-check"></i>
                            </label>
                    </div>
                </div>
                <div class="col-md-6"style="padding-left: 20px;">
                    <select id="inline_skating_level" name="inline_skating_level" class="form-control s_level" required {{ old('level', isset($selected_pupil)?explode(', ', $selected_pupil->level)[0]:'')==0?'disabled':'' }}>
                        <option value="" selected hidden="">Level</option>
                            @foreach($levels as $level)
                            <option value={{$level->level_id}} {{old('inline_skating_level', isset($selected_pupil)?explode(', ', $selected_pupil->level)[0]:'') == $level->level_id?'selected':''}} >{{$level->level_name}}</option>
                            @endforeach
                    </select>
                    <select id="crossminton_level" name="crossminton_level" class="form-control s_level" required {{ old('level', isset($selected_pupil)?explode(', ', $selected_pupil->level)[1]:'')==0?'disabled':'' }}>
                        <option value="" selected hidden="">Level</option>
                            @foreach($levels as $level)
                            <option value={{$level->level_id}} {{old('crossminton_level', isset($selected_pupil)?explode(', ', $selected_pupil->level)[1]:'') == $level->level_id?'selected':''}} >{{$level->level_name}}</option>
                            @endforeach
                    </select>
                    <select id="skateboarding_level" name="skateboarding_level" class="form-control s_level" required {{ old('skateboarding_level', isset($selected_pupil)?explode(', ', $selected_pupil->level)[2]:'')==0?'disabled':'' }}>
                        <option value="" selected hidden="">Level</option>
                            @foreach($levels as $level)
                            <option value={{$level->level_id}} {{old('skateboarding_level', isset($selected_pupil)?explode(', ', $selected_pupil->level)[2]:'') == $level->level_id?'selected':''}} >{{$level->level_name}}</option>
                            @endforeach
                    </select>
                    <select id="crousing_level" name="crousing_level" class="form-control s_level" required {{ old('level', isset($selected_pupil)?explode(', ', $selected_pupil->level)[3]:'')==0?'disabled':'' }}>
                        <option value="" selected hidden="">Level</option>
                            @foreach($levels as $level)
                            <option value={{$level->level_id}} {{old('crousing_level', isset($selected_pupil)?explode(', ', $selected_pupil->level)[3]:'') == $level->level_id?'selected':''}} >{{$level->level_name}}</option>
                            @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
            <label for="weight" class="col-md-4 control-label">Weight (kg.)</label>
            <div class="col-md-6">
                <input id="weight" type="number"  step="1" class="form-control" name="weight" value="{{ old('weight', isset($selected_pupil)?$selected_pupil->weight:'') }}" required autofocus>
                @if ($errors->has('weight'))
                        <span class="help-block">
                        <strong>{{ $errors->first('weight') }}</strong>
                        </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('height') ? ' has-error' : '' }}">
            <label for="height" class="col-md-4 control-label">Height (cm.)</label>

            <div class="col-md-6">
            <input id="height" type="text" class="form-control" name="height" value="{{ old('height', isset($selected_pupil)?$selected_pupil->height:'') }}" required autofocus>

            @if ($errors->has('height'))
                    <span class="help-block">
                    <strong>{{ $errors->first('height') }}</strong>
                    </span>
            @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('foot_size') ? ' has-error' : '' }}">
            <label for="foot_size" class="col-md-4 control-label">Foot size (EU)</label>

            <div class="col-md-6">
            <input id="foot_size" type="text" class="form-control" name="foot_size" value="{{ old('foot_size', isset($selected_pupil)?$selected_pupil->foot_size:'') }}" required autofocus>

            @if ($errors->has('foot_size'))
                    <span class="help-block">
                    <strong>{{ $errors->first('foot_size') }}</strong>
                    </span>
            @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary btn-block btn-conic">
                    Save
                </button>
            </div>
        </div>
    </form>

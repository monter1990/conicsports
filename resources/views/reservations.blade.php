@extends('layouts.app')

@section('content')
<div class="container auth-container">
    <nav class="transformer-tabs d-md-flex flex-md-row" id="" role='navigation'>
    {!! $MyNavBar->asUl(['class' => 'link-list', 'id' => 'myTab', 'role' => 'tablist']) !!}
    <div class="tab-content py-3 py-md-0">
            <div class="loading"><div class="loading-wheel"></div></div>
            <h1 class="conic-header">Time to...</h1>
    @include('flash-message')
    <div class="col-md-12" style="text-align: right;">
      <div class="legend"><div class="location_name_map_legend"><label class="legendlabel">Confirmed training location - &nbsp;&nbsp;</label></div><div class="legend_pointer confirmed"></div></div>
      <div class="legend"><div class="location_name_map_legend"><label class="legendlabel">Exact location to be confirmed - &nbsp;&nbsp;</label></div><div class="legend_pointer notconfirmed"></div></div>
      <div class="legend"><div class="location_name_map_legend"><label class="legendlabel">Summer camp 2018 - &nbsp;&nbsp;</label></div><div class="legend_pointer camp"></div></div>
    </div>
                <div class="content">
                  <div class="container-fluid" id="imagecont">

                    @foreach($locations as $malls)
                          <a class="pointer {{ $i=$i+1 }} @if($malls->confirmed == 1) confirmed @else notconfirmed @endif" data-location-name="{{ $malls->name_location }}" data-id-location="{{ $malls->id_location }}" href="{{ route('new_reservation').$malls->id_location.'&sport=1' }}">
                            <span class="pointer_span" style="display:none;">{{ $malls->name_location }}</span></a>
                    @endforeach
                    <img class="img-fluid" id="mapimage" src="http:\\localhost\conic\resources\map3.png">
                </div>              
        

                </div>
                <div class="tab-pane fade show active" id="dolor" role="tabpanel"></div>
        </div>
                  <div id="modal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog container-fluid" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                        </div>
                    </div>
                    </div>
                  </div> 
</nav>
</div>
@stop
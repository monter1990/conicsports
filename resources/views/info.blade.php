@extends('layouts.app')

@section('content')
<div class="container auth-container">
<nav class="transformer-tabs d-md-flex flex-md-row" id="" role='navigation'>
{!! $MyNavBar->asUl(['class' => 'link-list', 'id' => 'myTab', 'role' => 'tablist']) !!}
        <div class="tab-content py-3 py-md-0">
                <div class="loading">
                        <div class="loading-wheel"></div>
                </div>
                <h1 class="conic-header">Your personal information</h1>
                @include('flash-message')

                <div class="content">
                        <form class="form-horizontal" method="POST" action="{{ route('information') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">Gender</label>

                                        <div class="col-md-6">
                                        <label class="radio" for="Male">Male
            <input class="form-check-input" name="id_gender" type="radio" id="Male" value="0" required="" {{old('id_gender', $user->id_gender)==0?'checked':''}}>  <i class="checkround"></i>
        </label>

              <label class="radio" for="Female">Female
            <input class="form-check-input" name="id_gender" type="radio" id="Female" value="1" required="" {{old('id_gender', $user->id_gender)==1?'checked':''}}>  <i class="checkround"></i>
        </label>

                                        @if ($errors->has('gender'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('gender') }}</strong>
                                                </span>
                                        @endif
                                        </div>
                                </div>
                                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">First Name</label>

                                        <div class="col-md-6">
                                        <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name', $user->first_name) }}" required autofocus>

                                        @if ($errors->has('first_name'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                                </span>
                                        @endif
                                        </div>
                                </div>
                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">Last Name</label>

                                        <div class="col-md-6">
                                        <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name', $user->last_name) }}" required autofocus>

                                        @if ($errors->has('last_name'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                        @endif
                                        </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">Birthday</label>
                                        <div class="field">
                                                <div class="col-xs-4 days">
                                                        <select id="days" name="days" value="{{ old('days') }}" class="form-control" required="">
                                                                <option value="" disabled="" selected hidden="">Day</option>
                                                                        @foreach($days as $index => $day)
                                                                        <option value={{$index}} {{($user->birthday==null?'':old('days', explode('-', $user->birthday)[2]) == $index?'selected':'')}} >{{$day}}</option>
                                                                        @endforeach
                                                        </select>
                                                </div>
                                                                        
                                                <div class="col-xs-4 months">
                                                        <select id="months" name="months" class="form-control"  required="">
                                                                <option value="" disabled="" selected hidden="">Month</option>
                                                                        @foreach($months as $index => $month)
                                                                        <option value={{$index}} {{$user->birthday==null?'':(old('months', explode('-', $user->birthday)[1]) == $index?'selected':'')}} >{{$month}}</option>
                                                                        @endforeach
                                                        </select>
                                                </div>
                                                <div class="col-xs-4 years">
                                                        <select id="years" name="years" class="form-control"  selected="{{ old('years') }}" required="">
                                                                <option value="" disabled="" selected hidden="">Year</option>
                                                                        @foreach($years as $index => $year)
                                                                        <option value={{$index}} {{$user->birthday==null?'':(old('years', explode('-', $user->birthday)[0]) == $index?'selected':'')}} >{{$year}}</option>
                                                                        @endforeach
                                                        </select>
                                                </div>
                                        </div>
                                </div>    
                                <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                                <label for="country" class="col-md-4 control-label">Country</label>

                                                <div class="col-md-6">
                                                        <select id="country" name="country" class="form-control"  selected="{{ old('country') }}" required="">
                                                                <option value="" disabled="" selected hidden="">Country</option>
                                                                        @foreach($countries as $index => $country)
                                                                        <option value={{$index}} {{(old('country', $user->country) == $index?'selected':'')}} >{{$country}}</option>
                                                                        @endforeach
                                                        </select>

                                                @if ($errors->has('country'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('country') }}</strong>
                                                        </span>
                                                @endif
                                                </div>
                                </div>
                                <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                                                <label for="street" class="col-md-4 control-label">Street</label>

                                                <div class="col-md-6">
                                                <input id="street" type="text" class="form-control" name="street" value="{{ old('street', $user->street) }}" required>
                                                @if ($errors->has('street'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('street') }}</strong>
                                                        </span>
                                                @endif
                                                </div>
                                </div>
                                <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                                                <label for="number" class="col-md-4 control-label">№</label>

                                                <div class="col-md-6">
                                                <input id="number" type="number" class="form-control" name="number" value="{{ old('number', $user->number) }}" required>

                                                @if ($errors->has('number'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('number') }}</strong>
                                                        </span>
                                                @endif
                                                </div>
                                </div>
                                <div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">
                                                <label for="street" class="col-md-4 control-label">Zip Code</label>

                                                <div class="col-md-6">
                                                <input id="zipcode" type="text" class="form-control" name="zip_code" value="{{ old('zip_code', $user->zip_code) }}" required>
                                                @if ($errors->has('zip_code'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('zip_code') }}</strong>
                                                        </span>
                                                @endif
                                                </div>
                                </div>
                                <div class="form-group{{ $errors->has('town') ? ' has-error' : '' }}">
                                                <label for="number" class="col-md-4 control-label">Town / Village</label>

                                                <div class="col-md-6">
                                                <input id="town" type="text" class="form-control" name="town" value="{{ old('town', $user->town) }}" required>

                                                @if ($errors->has('town'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('town') }}</strong>
                                                        </span>
                                                @endif
                                                </div>
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                                <div class="col-md-6">
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email', $user->email) }}" required>

                                                @if ($errors->has('email'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                @endif
                                                </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                                <label for="email" class="col-md-4 control-label">Phone Number</label>

                                                <div class="col-md-6">
                                                <input id="phone_number" type="tel" class="form-control" name="phone_number" value="{{ old('phone_number', $user->phone_number) }}" pattern="^[0-9-+s()]*$" title="Phone number should consist of digits" required>

                                                @if ($errors->has('phone_number'))
                                                        <span class="help-block">
                                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                                        </span>
                                                @endif
                                                </div>
                                </div>
                                <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary btn-block btn-conic">
                                                        Save
                                                </button>
                                        </div>
                                </div>
                        </form>
                </div>
                <div class="tab-pane fade show active" id="dolor" role="tabpanel"></div>
        </div>
</nav>
</div>
@endsection








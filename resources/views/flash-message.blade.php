@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif


@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif


@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{{ $message }}</strong>
</div>
@endif

@if ($message = Session::get('info'))
<div class="alert alert-info alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{{ $message }}</strong>
</div>
@endif

@if ($message = Session::get('registration_info'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
  </div>
  @if (null!==Session::get('pupils_deleted_applications'))
    <div class='alert alert-warning'>
      <button type='button' class='close' data-dismiss='alert'>×</button>
      <strong>Following regestrations were deleted</strong>
      <ul class='del_regis'>
      @if(count(Session::get('pupils_deleted_applications'))>0)
        @foreach (Session::get('pupils_deleted_applications') as $index => $deleted_application)
            <li> {{ ($index+1).'. '.$deleted_application }} </li>
        @endforeach
      @endif
      </ul>
    </div>
  @endif
@endif

@if ($errors->any())
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	Please check the form below for errors
</div>
@endif
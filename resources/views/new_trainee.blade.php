@extends('layouts.app')

@section('content')
<div class="container auth-container">
                <nav class="transformer-tabs d-md-flex flex-md-row" id="" role='navigation'>
                {!! $MyNavBar->asUl(['class' => 'link-list', 'id' => 'myTab', 'role' => 'tablist']) !!}
                <div class="tab-content py-3 py-md-0">
                        <div class="loading"><div class="loading-wheel"></div></div>
                        <h1 class="conic-header">New Trainee</h1>
                @include('flash-message')

                @if(!isset($fresh))
                      <div class="button_div_left">            
                          <a class="btn addbutton backbutton" href="{{ route('pupils') }}"><i class="fas fa-arrow-left"></i><p>Back</p></a>
                     </div>
                @endif
                <div class="content">
              
                        @include('partials.pupil_form')
                </div>
                <div class="tab-pane fade show active" id="dolor" role="tabpanel"></div>
        </div>
</nav>
</div>
@endsection








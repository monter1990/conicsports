@extends('layouts.app')

@section('content')
    <div class="container auth-container">
      <nav class="transformer-tabs d-md-flex flex-md-row" id="" role='navigation'>
        {!! $MyNavBar->asUl(['class' => 'link-list', 'id' => 'myTab', 'role' => 'tablist']) !!}
        <div class="tab-content py-3 py-md-0">
          <h1 class="conic-header"><div id="location_title" style="color:{{$location[0]->color}}"><i class="fas fa-crosshairs"></i></div>{{ $location[0]->name_location }}</h1>
          <div class="button_div_left">            
            <a class="btn addbutton backbutton" href="{{ route('reservations') }}"><i class="fas fa-arrow-left"></i><p>Back</p></a>
          </div>
          @include('flash-message')
          <div class="content" style="width:100%">
            <div class="loading"><div class="loading-wheel"></div></div>
            <div id="modal" class="modal fade" tabindex="-1" role="dialog">
              <div class="modal-dialog container-fluid" role="document">
                <div class="modal-content">
                <div class="loading"><div class="loading-wheel"></div></div>
                  <div class="modal-header modal-header-delete">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                      <h5 class="modal-title" id="exampleModalLongTitle">Select trainees to apply.</h5>
                  </div>
                    <form class="form-horizontal" method="POST" action="{{ route($route) }}">
                      {{ csrf_field() }}
                      <div class="modal-body modal-delete">
                        <div class="flash-message"></div>
                          <b name="location" id="{{$location[0]->id_location}}">Location: {{$location[0]->name_location}}</b><br>
                          <input type="hidden" name="location" value="{{$location[0]->id_location}}">
                          <b id="sport">Sport:</b><br>
                          <input type="hidden" name="sport" value="">
                          <b name="day" data-id="" id="day">Day:</b><br>
                          <input type="hidden" name="day" value="">
                          <b name="time" data-id="" id="time">Time:</b><br>
                          <input type="hidden" name="time" value="">
                          @if(count($pupils_data)>0)
                            <div class="checkbox">
                              <label class="sport_all" for="appliedall">
                                <input type="checkbox" id="appliedall" name="appliedall">Select All
                                <i class="fas fa-check"></i>
                              </label>
                            </div>
                            <div style="width:100%;background:#7d7c7c;max-height:200px;overflow-x: auto;">
                              <ul class="pupilsul" style="width:100%;padding-right: 5px;">
                                @foreach($pupils_data as $index => $pupil)
                                  <li class="pupilsli">
                                  <div class="checkbox regist">
                                    <label class="sport" for="applied_{{ $pupil->id }}">
                                      <input type="checkbox" id="applied_{{ $pupil->id }}" name="applied_{{ $pupil->id }}" value="{{ $pupil->id }}"> {{ ($index+1).'. '.$pupil->first_name.' '.$pupil->last_name}}
                                      <i class="fas fa-check"></i>
                                    </label>
                                    </div>
                                  </li>
                                @endforeach
                              </ul>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-cancel" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success btn-conic">Apply</button>
                          </div>
                            @else
                              <div class="modal-header modal-header-delete" style="width:100%;white-space: normal;">
                                <h5 class="modal-title" id="exampleModalLongTitle">No trainees registered for this sport. Please check "Trainees" section in the menu.</h5>
                              </div>
                            </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-danger btn-cancel" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-success btn-conic" disabled>Apply</button>
                              </div>    
                            @endif
                    </form>
                </div>
              </div>
            </div> 
            <div class="modal-body">
              <div class="modal-body-wrapper">
                <ul class="nav nav-tabs modal-nav-tabs" id="myTab1" role="tablist">
                  @foreach($sport_categories as $index => $category)
                    <li class="modal-nav-item modal-nav-item {{$index==($_GET['sport']-1)? 'active' : ''}}">
                      <a class="modal-nav-link modal-nav-link " data-name="{{ $category->category_name }}" href="{{ route('new_reservation').$location[0]->id_location.'&sport='.$category->category_id }}" id="{{ $category->category_id }}">
                        <span class="sport_icon"><img src="http:\\localhost\conic\resources\{{ $category->icon }}"></span>
                        @foreach($pupils as $index_check=>$pupil_check)
                          @if($pupil_check->sport_category == $category->category_id)
                            <i class="fas fa-check in_modal"></i>
                            @break
                          @endif
                        @endforeach
                      </a>
                    </li>
                  @endforeach
                </ul>
              <div class="row times">
                <div class="table_div col">
                  <table class="table table-timeframe-apply">
                    <tr>
                      <th class="field-label col-xs-3 active">
                        <label>Time Frames</label>
                      </th>
                      <th class="field-label col-xs-3 active">
                        <label>Monday</label>
                      </th> 
                      <th class="field-label col-xs-3 active">
                        <label>Tuesday</label>
                      </th> 
                      <th class="field-label col-xs-3 active">
                        <label>Wednesday</label>
                      </th> 
                      <th class="field-label col-xs-3 active">
                        <label>Thursday</label>
                      </th> 
                      <th class="field-label col-xs-3 active">
                        <label>Friday</label>
                      </th> 
                      <th class="field-label col-xs-3 active">
                        <label>Saturday</label>
                      </th> 
                      <th class="field-label col-xs-3 active">
                        <label>Sunday</label>
                      </th>
                    </tr>
                    @foreach($time_frames as $time_frame)
                      <tr>
                        <td class="field-label col-xs-3 age_cat">
                          <label>{{ $time_frame->time_frame }}</label>
                        </td>
                        @for ($i=0; $i < 7; $i++)
                          <td data-registered="@foreach($registered as $index_r=>$pupil_r)@if($pupil_r->time_frame==$time_frame->id && $pupil_r->week_day==($i+1)){{$pupil_r->pupil.','}}@endif @endforeach" class="col-9 plus_plus" data-day="{{$i+1}}" data-timeid="{{ $time_frame->id }}" data-time="{{ $time_frame->time_frame }}">
                            <i class="fas fa-plus" style="display: none"></i>
                              @if($count_array[$time_frame->id][$i+1]!=0)
                                @foreach($registered as $index_r=>$pupil_r)
                                  @if($pupil_r->sport_category == $_GET['sport'])
                                    <label class="legend_localations {{'inside_table _'.$location[0]->id_location}}" style="background-color:{{$location[0]->color}}">
                                    {{ $count_array[$time_frame->id][$i+1]}}
                                    </label>
                                  @endif
                                  @break
                                @endforeach
                              @endif
                          </td>
                        @endfor
                      </tr>
                    @endforeach
                  </table>
                  </div>
                </div>
              </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade show active" id="dolor" role="tabpanel"></div>
        </div>
      </nav>
    </div>
@endsection

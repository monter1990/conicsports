@extends('layouts.app')

@section('content')
<div class="container auth-container">
    <nav class="transformer-tabs d-md-flex flex-md-row" id="" role='navigation'>
    {!! $MyNavBar->asUl(['class' => 'link-list', 'id' => 'myTab', 'role' => 'tablist']) !!}
    <div class="tab-content py-3 py-md-0">
            <div class="loading"><div class="loading-wheel"></div></div>
            <h1 class="conic-header">Registered trainees</h1>
    @include('flash-message')
    <div id="modal_delete" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header modal-header-delete">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body modal-delete">
            <h3 class="md_cont">Do you want to procceed with deleting?</br> All registrations will be lost.</h3>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-danger">Delete</button>
          </div>
        </div>
      </div>
    </div> 
                
                      <div class="button_div_right">            
                          <button class="btn addbutton trashbutton" style="color:#9b0202"><i class="fas fa-trash"></i></button>
                          <a class="btn addbutton plusbutton" href="{{ route('new_trainee') }}"><i class="fas fa-plus"></i></a>
                     </div>
                  <ul class="pupilsul">
                        @foreach($pupils_data as $index => $pupil)
                                @if ($selected_pupil->id==$pupil->id)
                                <li class="pupilsli"> <span id="{{ $pupil->id }}" class="active">{{ ($index+1).'. '.$pupil->first_name.' '.$pupil->last_name}}</span> </li>
                                @else
                                <li class="pupilsli"> <span id="{{ $pupil->id }}" class="">{{ ($index+1).'. '.$pupil->first_name.' '.$pupil->last_name}}</span> </li>
                                @endif
                        @endforeach

                  </ul>
                <div class="content">
              
                        @include('partials.pupil_form')

                </div>
                <div class="tab-pane fade show active" id="dolor" role="tabpanel"></div>
        </div>
</nav>
</div>
@stop








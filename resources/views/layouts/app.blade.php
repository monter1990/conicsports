<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/v4-shims.css">
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="layoutdiv">
      <div class="col-6 top"><p class="leading" style="    font-family: 'Anton', sans-serif;
        font-family: 'Roboto', sans-serif;">Enroll To Roll In.</p></div>
      <nav class="navbar navbar-expand-lg navbar-light bg-light " background="red">
          <div class="container containernav">
          <a class="navbar-brand navbar-brand-logo abs-center-x" href="http://localhost/prestadebug/">
      <img class="img-fluid" src="http:\\localhost\conic\resources\conic_logo_tr.png">
      </a>
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav">
                            &nbsp;
                        </ul>
    
                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @guest
                                <li><a href="{{ route('login') }}">Login</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
    
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>
    
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            @endguest
                        </ul>
                    </div>
                  <div class="redline col-12"></div>
      </div>
      </nav>
      <div class="col-6 bottom"><p class="leading"> </p></div>

                    




  {{--                 <div class="col-6 top"><p class="leading">Enroll To Roll In.</p></div>
                <nav class="navbar navbar-expand-lg navbar-light bg-light " background="red">
            <div class="container">
                <div class="navbar-header ">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <div class="container containernav">
                      <a class="navbar-brand navbar-brand-logo abs-center-x" href="http://localhost/prestadebug/">
                   <img class="img-fluid" src="http:\\localhost\conic\resources\conic_logo_tr.png">
                   </a>
                                      
                  </div>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <div class="col-6 bottom"><p class="leading"> </p></div>
      --}}
    @yield('content')
</div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
<footer id="footer">
  <div style="text-align: center;">
      <div class="block-contact">
        <h4 style="margin-top:10px">Contact us<br></h4>
        @if($contacts->adress!='')
          <p style="color:#666666;margin-bottom: .5rem;text-align: left;">{{ $contacts->adress }}</p>
        @endif
        @if($contacts->phone!='')
          <p style="color:#666666;margin-bottom: .5rem;text-align: left;">Tel: {{ $contacts->phone }}</p>
        @endif
        @if($contacts->fax!='')
          <p style="color:#666666;margin-bottom: .5rem;text-align: left;">Fax: {{ $contacts->fax }}</p>
        @endif
        @if($contacts->mobile1!='')
          <p style="color:#666666;margin-bottom: .5rem;text-align: left;">Mobile: {{$contacts->mobile1}}@if($contacts->mobile2!='') / {{$contacts->mobile2}}@endif</p>
        @endif
        @if($contacts->email!='')
          <p style="color:#666666;margin-bottom: .5rem;text-align: left;">Email: {{$contacts->email}}</p>
          @endif
      </div>
      <div class="block-contact">
        <h4 style="margin-top:10px">Us on social media<br></h4>
        <a href="https://www.facebook.com/conicsports"><img class="social_logo" src="http://mauroller.com/img/facebook_logo.png" style="width:59px;height:59px;margin-right:15px;"></img></a>
        <a href="https://api.whatsapp.com/send?phone=23059633090&text='Hi, I contacted you Through your website.'"><img class="social_logo" src="http://mauroller.com/img/whatsapp_logo.png" style="width:50px;height:50px;"></img></a>
        <p style="color:#666666;margin-bottom: .5rem;"> </p>
        </div>
      </div>
      <p style="color:black;padding-left: 10px;" class="_blank" href="http://www.prestashop.com" target="_blank">
        © 2018 - Conic Sports™
      </p>
</footer>
</html>

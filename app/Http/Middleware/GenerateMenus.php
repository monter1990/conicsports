<?php

namespace App\Http\Middleware;

use Closure;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Menu::make('MyNavBar', function ($menu) {
            $menu->group(['class' => 'nav-item'], function($m){
                $m->add('Information', ['route'  => 'information'])->link->attr(['class' => 'nav-link curve_decoration']);
                $m->add('Trainees', ['route'  => 'pupils'])->link->attr(['class' => 'nav-link curve_decoration']);
                $m->add('Reservation', ['route'  => 'reservations'])->link->attr(['class' => 'nav-link curve_decoration']);
            });
        });
            return $next($request);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;

class ReservationsController extends Controller
{
    public function index(){
      $pupils_data = \DB::table('pupils')->where('id_user', \Auth::id())->get();
      $route = 'registration';
      if(isset($_GET['location'])){
        $count_array = [];
        $time_frames = \DB::table('timeframes')->get();
        foreach($time_frames as $time_frame){
          for($i=1; $i < 8; $i++){
            $pupils_count_t = \DB::table('ps_camp_applications')->where(['location' => $_GET['location'], 'time_frame' => $time_frame->id, 'week_day' => $i, 'sport_category' => $_GET['sport']])->get()->count();
            $count_array[$time_frame->id][$i]=$pupils_count_t;
          }
        }
//        print_r($count_array);
        foreach($pupils_data as $pupil_index => $pupil_values){
  //      print_r($pupil_values);
          $pupil_sport = explode(', ', $pupil_values->sport);
          if($pupil_sport[($_GET['sport']-1)]!=1){
            unset($pupils_data[$pupil_index]);
          }
        }
        $pupils_data = $pupils_data->toArray();
        $pupils_data = array_values($pupils_data);
 //       print_r($pupils_data);
        $new_view = view('new_reservation', [
            'registered' => \DB::table('ps_camp_applications')->where(['id_user' => \Auth::id(), 'location' => $_GET['location'], 'sport_category' => $_GET['sport']])->get(),
            'count_array' => $count_array,
            'pupils' => \DB::table('ps_camp_applications')->where(['location' => $_GET['location']])->get(),
            'location' => \DB::table('locations')->where(['id_location' => $_GET['location']])->get(),
            'pupils_data' => $pupils_data,
            'sport_categories' => \DB::table('sports_category')->get(),
            'age_categories' => \DB::table('age_category')->get(),
            'time_frames' => $time_frames,
            'my_pupils_count' => \DB::table('pupils')->where(['id_user' => \Auth::id()])->get()->count(),
            'age_to' => 1,
            'j' => 0,
            'levels' => \DB::table('levels')->get(),
            'route' => $route
          ]); 
        }else{
            $new_view = view('reservations', [
                'locations' => \DB::table('locations')->get(),
                'route' => $route,
                'i' => 0
            ]);  
        }
      return $new_view;
    }

    public function register_pupil(){
      $pupils_data = \DB::table('pupils')->where('id_user', \Auth::id())->get();
      $route = 'registration';
      $pupils_age_catogories = [];
      $all_pupils_ids = [];
      $pupils = [];
//        $pupil_application = new Application();
      foreach($_POST as $name => $value){
        if(strpos($name, 'applied_')!==FALSE){
          $pupils[]=$value;
        }
      }
      $all_pupils = \DB::table('ps_camp_applications')->where([
            'id_user' => \Auth::id(), 
            'sport_category' => $_POST['sport'], 
            'location' => $_POST['location'],
            'time_frame' => $_POST['time'],
            'week_day' => $_POST['day']
        ])->get(); // All camp_applications registered

        $all_pupils = $all_pupils->toArray();
        for($k=0;$k<count($all_pupils);$k++){
            $all_pupils_ids[$k] = $all_pupils[$k]->pupil;
        }
//            print_r($all_pupils_ids);
//            print('next_pupils');

        if(count($pupils)>0){
          foreach($pupils as $key => $value){
            $pupil_data = \DB::table('pupils')->where(['id_user' => \Auth::id(), 'id' => $value])->get();
            $level = explode(', ', $pupil_data[0]->level)[($_POST['sport']-1)];
            if (($key = array_search($value, $all_pupils_ids)) !== false) {
              unset($all_pupils_ids[$key]);
              $pupils_age_catogories = array_values($all_pupils_ids);
            }

/****** If such application allready exists. *******/
            if((\DB::table('ps_camp_applications')->where([
              'id_user' => \Auth::id(),
              'pupil' => $value,
              'sport_category' => $_POST['sport'], 
              'location' => $_POST['location'],
              'time_frame' => $_POST['time'],
              'week_day' => $_POST['day']
            ])->get()->count()==0)){
              \DB::table('ps_camp_applications')->insert(
                array('id_user' => \Auth::id(),
                'location' => $_POST['location'],
                'pupil' => $value,
                'sport_category' => $_POST['sport'],
                'level' => $level,
                'time_frame' => $_POST['time'],
                'week_day' => $_POST['day']
                )
              );
            }
          }
/****** If there are applications to delete. *******/
          if(count($pupils_age_catogories)>0){
            $pupils_deleted_applications = [];
            foreach($pupils_age_catogories as $index => $pupil_notexists_id){
                \DB::table('ps_camp_applications')->where([
                    'id_user' => \Auth::id(),
                    'location' => $_POST['location'],
                    'pupil' => $pupil_notexists_id,
                    'sport_category' => $_POST['sport']
                ])->delete();
                $deleted_pupil_applications = \DB::table('pupils')->where(['id_user' => \Auth::id(), 'id' => $pupil_notexists_id])->get();
                $pupils_deleted_applications[] = $deleted_pupil_applications[0]->first_name.' '.$deleted_pupil_applications[0]->last_name;
            }
            return redirect()->route('reservations', ['location' => $_POST['location'], 'sport' => $_POST['sport']])->with('pupils_deleted_applications', $pupils_deleted_applications);
          }else{
            return redirect()->route('reservations', ['location' => $_POST['location'], 'sport' => $_POST['sport']])->with('registration_info','New Registration was made. Thank you.');
          }
        }else{
/****** If there are applications to delete. *******/
          if(count($all_pupils_ids)>0){
            $pupils_deleted_applications = [];
            foreach($all_pupils_ids as $index => $pupil_notexists_id){
                \DB::table('ps_camp_applications')->where([
                    'id_user' => \Auth::id(),
                    'location' => $_POST['location'],
                    'pupil' => $pupil_notexists_id,
                    'sport_category' => $_POST['sport'],
                    'time_frame' => $_POST['time'],
                    'week_day' => $_POST['day']    
                ])->delete();
                $deleted_pupil_applications = \DB::table('pupils')->where(['id_user' => \Auth::id(), 'id' => $pupil_notexists_id])->get();
                $pupils_deleted_applications[] = $deleted_pupil_applications[0]->first_name.' '.$deleted_pupil_applications[0]->last_name;
            }
            return redirect()->route('reservations', ['location' => $_POST['location'], 'sport' => $_POST['sport']])->with('pupils_deleted_applications', $pupils_deleted_applications);
          }else{
            return redirect()->route('reservations', ['location' => $_POST['location'], 'sport' => $_POST['sport']])->with('registration_info','New Registration was made. Thank you.');
          }
        }
        /*else{
//            print("day\n");
//            print_r($_POST['day']);
            $del_applications = \DB::table('ps_camp_applications')->where([
                'id_user' => \Auth::id(),
                'location' => $_POST['location'],
                'sport_category' => $_POST['sport'],
                'time_frame' => $_POST['time'],
                'week_day' => $_POST['day']
            ])->get();
            $pupils_deleted_applications = [];
            $l = 0;
            foreach($del_applications as $value){
                $deleted_pupil_applications = \DB::table('pupils')->where(['id_user' => \Auth::id(), 'id' => $value->pupil])->get();
                $pupils_deleted_applications[$l] = $deleted_pupil_applications[0]->first_name.' '.$deleted_pupil_applications[0]->last_name;
                $l++;
            }
            \DB::table('ps_camp_applications')->where([
                'id_user' => \Auth::id(),
                'location' => $_POST['location'],
                'sport_category' => $_POST['sport'],
                'time_frame' => $_POST['time'],
                'week_day' => $_POST['day']
            ])->delete();
            return $pupils_deleted_applications;
        }
      */
      }
}
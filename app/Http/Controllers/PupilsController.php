<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pupil;
use Illuminate\Support\Facades\Auth;

class PupilsController extends Controller
{
   public $select_values = [];
      public function __construct()
      {
        $class_values = [ 'days' => array('01' => '1','02' => '2','03' => '3','04' => '4','05' => '5','06' => '6','07' => '7',
        '08' => '8','09' => '9','10' => '10','11' => '11','12' => '12','13' => '13','14' => '14','15' => '15','16' => '16',
        '17' => '17','18' => '18','19' => '19','20' => '20','21' => '21','22' => '22','23' => '23','24' => '24','25' => '25',
        '26' => '26','27' => '27','28' => '28','29' => '29','30' => '30','31' => '31'), 
      'months' => array('01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', 
                      '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December'),
      'years' => array_combine(range(date("Y + 1 year"), 1930), range(date("Y + 1 year"), 1930)),
      'countries' => array("Mauritius"=>"Mauritius","Réunion"=>"Réunion","Madagascar"=>"Madagascar","Afghanistan"=>"Afghanistan","Albania"=>"Albania","Algeria"=>"Algeria","American Samoa"=>"American Samoa","Andorra"=>"Andorra","Angola"=>"Angola","Anguilla"=>"Anguilla","Antarctica"=>"Antarctica","Antigua and Barbuda"=>"Antigua and Barbuda","Argentina"=>"Argentina","Armenia"=>"Armenia","Aruba"=>"Aruba","Australia"=>"Australia","Austria"=>"Austria","Azerbaijan"=>"Azerbaijan","Bahamas"=>"Bahamas","Bahrain"=>"Bahrain","Bangladesh"=>"Bangladesh","Barbados"=>"Barbados","Belarus"=>"Belarus","Belgium"=>"Belgium","Belize"=>"Belize","Benin"=>"Benin","Bermuda"=>"Bermuda","Bhutan"=>"Bhutan","Bolivia"=>"Bolivia","Bosnia and Herzegovina"=>"Bosnia and Herzegovina","Botswana"=>"Botswana","Bouvet Island"=>"Bouvet Island","Brazil"=>"Brazil","British Antarctic Territory"=>"British Antarctic Territory","British Indian Ocean Territory"=>"British Indian Ocean Territory","British Virgin Islands"=>"British Virgin Islands","Brunei"=>"Brunei","Bulgaria"=>"Bulgaria","Burkina Faso"=>"Burkina Faso","Burundi"=>"Burundi","Cambodia"=>"Cambodia","Cameroon"=>"Cameroon","Canada"=>"Canada","Canton and Enderbury Islands"=>"Canton and Enderbury Islands","Cape Verde"=>"Cape Verde","Cayman Islands"=>"Cayman Islands","Central African Republic"=>"Central African Republic","Chad"=>"Chad","Chile"=>"Chile","China"=>"China","Christmas Island"=>"Christmas Island","Cocos [Keeling] Islands"=>"Cocos [Keeling] Islands","Colombia"=>"Colombia","Comoros"=>"Comoros","Congo - Brazzaville"=>"Congo - Brazzaville","Congo - Kinshasa"=>"Congo - Kinshasa","Cook Islands"=>"Cook Islands","Costa Rica"=>"Costa Rica","Croatia"=>"Croatia","Cuba"=>"Cuba","Cyprus"=>"Cyprus","Czech Republic"=>"Czech Republic","Côte d’Ivoire"=>"Côte d’Ivoire","Denmark"=>"Denmark","Djibouti"=>"Djibouti","Dominica"=>"Dominica","Dominican Republic"=>"Dominican Republic","Dronning Maud Land"=>"Dronning Maud Land","East Germany"=>"East Germany","Ecuador"=>"Ecuador","Egypt"=>"Egypt","El Salvador"=>"El Salvador","Equatorial Guinea"=>"Equatorial Guinea","Eritrea"=>"Eritrea","Estonia"=>"Estonia","Ethiopia"=>"Ethiopia","Falkland Islands"=>"Falkland Islands","Faroe Islands"=>"Faroe Islands","Fiji"=>"Fiji","Finland"=>"Finland","France"=>"France","French Guiana"=>"French Guiana","French Polynesia"=>"French Polynesia","French Southern Territories"=>"French Southern Territories","French Southern and Antarctic Territories"=>"French Southern and Antarctic Territories","Gabon"=>"Gabon","Gambia"=>"Gambia","Georgia"=>"Georgia","Germany"=>"Germany","Ghana"=>"Ghana","Gibraltar"=>"Gibraltar","Greece"=>"Greece","Greenland"=>"Greenland","Grenada"=>"Grenada","Guadeloupe"=>"Guadeloupe","Guam"=>"Guam","Guatemala"=>"Guatemala","Guernsey"=>"Guernsey","Guinea"=>"Guinea","Guinea-Bissau"=>"Guinea-Bissau","Guyana"=>"Guyana","Haiti"=>"Haiti","Heard Island and McDonald Islands"=>"Heard Island and McDonald Islands","Honduras"=>"Honduras","Hong Kong SAR China"=>"Hong Kong SAR China","Hungary"=>"Hungary","Iceland"=>"Iceland","India"=>"India","Indonesia"=>"Indonesia","Iran"=>"Iran","Iraq"=>"Iraq","Ireland"=>"Ireland","Isle of Man"=>"Isle of Man","Israel"=>"Israel","Italy"=>"Italy","Jamaica"=>"Jamaica","Japan"=>"Japan","Jersey"=>"Jersey","Johnston Island"=>"Johnston Island","Jordan"=>"Jordan","Kazakhstan"=>"Kazakhstan","Kenya"=>"Kenya","Kiribati"=>"Kiribati","Kuwait"=>"Kuwait","Kyrgyzstan"=>"Kyrgyzstan","Laos"=>"Laos","Latvia"=>"Latvia","Lebanon"=>"Lebanon","Lesotho"=>"Lesotho","Liberia"=>"Liberia","Libya"=>"Libya","Liechtenstein"=>"Liechtenstein","Lithuania"=>"Lithuania","Luxembourg"=>"Luxembourg","Macau SAR China"=>"Macau SAR China","Macedonia"=>"Macedonia","Malawi"=>"Malawi","Malaysia"=>"Malaysia","Maldives"=>"Maldives","Mali"=>"Mali","Malta"=>"Malta","Marshall Islands"=>"Marshall Islands","Martinique"=>"Martinique","Mauritania"=>"Mauritania","Mayotte"=>"Mayotte","Metropolitan France"=>"Metropolitan France","Mexico"=>"Mexico","Micronesia"=>"Micronesia","Midway Islands"=>"Midway Islands","Moldova"=>"Moldova","Monaco"=>"Monaco","Mongolia"=>"Mongolia","Montenegro"=>"Montenegro","Montserrat"=>"Montserrat","Morocco"=>"Morocco","Mozambique"=>"Mozambique","Myanmar [Burma]"=>"Myanmar [Burma]","Namibia"=>"Namibia","Nauru"=>"Nauru","Nepal"=>"Nepal","Netherlands"=>"Netherlands","Netherlands Antilles"=>"Netherlands Antilles","Neutral Zone"=>"Neutral Zone","New Caledonia"=>"New Caledonia","New Zealand"=>"New Zealand","Nicaragua"=>"Nicaragua","Niger"=>"Niger","Nigeria"=>"Nigeria","Niue"=>"Niue","Norfolk Island"=>"Norfolk Island","North Korea"=>"North Korea","North Vietnam"=>"North Vietnam","Northern Mariana Islands"=>"Northern Mariana Islands","Norway"=>"Norway","Oman"=>"Oman","Pacific Islands Trust Territory"=>"Pacific Islands Trust Territory","Pakistan"=>"Pakistan","Palau"=>"Palau","Palestinian Territories"=>"Palestinian Territories","Panama"=>"Panama","Panama Canal Zone"=>"Panama Canal Zone","Papua New Guinea"=>"Papua New Guinea","Paraguay"=>"Paraguay","People's Democratic Republic of Yemen"=>"People's Democratic Republic of Yemen","Peru"=>"Peru","Philippines"=>"Philippines","Pitcairn Islands"=>"Pitcairn Islands","Poland"=>"Poland","Portugal"=>"Portugal","Puerto Rico"=>"Puerto Rico","Qatar"=>"Qatar","Romania"=>"Romania","Russia"=>"Russia","Rwanda"=>"Rwanda","Saint Barthélemy"=>"Saint Barthélemy","Saint Helena"=>"Saint Helena","Saint Kitts and Nevis"=>"Saint Kitts and Nevis","Saint Lucia"=>"Saint Lucia","Saint Martin"=>"Saint Martin","Saint Pierre and Miquelon"=>"Saint Pierre and Miquelon","Saint Vincent and the Grenadines"=>"Saint Vincent and the Grenadines","Samoa"=>"Samoa","San Marino"=>"San Marino","Saudi Arabia"=>"Saudi Arabia","Senegal"=>"Senegal","Serbia"=>"Serbia","Serbia and Montenegro"=>"Serbia and Montenegro","Seychelles"=>"Seychelles","Sierra Leone"=>"Sierra Leone","Singapore"=>"Singapore","Slovakia"=>"Slovakia","Slovenia"=>"Slovenia","Solomon Islands"=>"Solomon Islands","Somalia"=>"Somalia","South Africa"=>"South Africa","South Georgia and the South Sandwich Islands"=>"South Georgia and the South Sandwich Islands","South Korea"=>"South Korea","Spain"=>"Spain","Sri Lanka"=>"Sri Lanka","Sudan"=>"Sudan","Suriname"=>"Suriname","Svalbard and Jan Mayen"=>"Svalbard and Jan Mayen","Swaziland"=>"Swaziland","Sweden"=>"Sweden","Switzerland"=>"Switzerland","Syria"=>"Syria","São Tomé and Príncipe"=>"São Tomé and Príncipe","Taiwan"=>"Taiwan","Tajikistan"=>"Tajikistan","Tanzania"=>"Tanzania","Thailand"=>"Thailand","Timor-Leste"=>"Timor-Leste","Togo"=>"Togo","Tokelau"=>"Tokelau","Tonga"=>"Tonga","Trinidad and Tobago"=>"Trinidad and Tobago","Tunisia"=>"Tunisia","Turkey"=>"Turkey","Turkmenistan"=>"Turkmenistan","Turks and Caicos Islands"=>"Turks and Caicos Islands","Tuvalu"=>"Tuvalu","U.S. Minor Outlying Islands"=>"U.S. Minor Outlying Islands","U.S. Miscellaneous Pacific Islands"=>"U.S. Miscellaneous Pacific Islands","U.S. Virgin Islands"=>"U.S. Virgin Islands","Uganda"=>"Uganda","Ukraine"=>"Ukraine","Union of Soviet Socialist Republics"=>"Union of Soviet Socialist Republics","United Arab Emirates"=>"United Arab Emirates","United Kingdom"=>"United Kingdom","United States"=>"United States","Unknown or Invalid Region"=>"Unknown or Invalid Region","Uruguay"=>"Uruguay","Uzbekistan"=>"Uzbekistan","Vanuatu"=>"Vanuatu","Vatican City"=>"Vatican City","Venezuela"=>"Venezuela","Vietnam"=>"Vietnam","Wake Island"=>"Wake Island","Wallis and Futuna"=>"Wallis and Futuna","Western Sahara"=>"Western Sahara","Yemen"=>"Yemen","Zambia"=>"Zambia","Zimbabwe"=>"Zimbabwe")
        ];
         $this->select_values = $class_values;
        }       
  
    public function index(Request $request){
      $pupils_data = \DB::table('pupils')->where('id_user', \Auth::id())->get();

        if(count($pupils_data)>0){
          if(isset($_GET['new'])){
 //           print_r($_GET);
            $route = 'pupil_ins';
            return view('new_trainee', [
              'select_values' => $this->select_values,
              'route' => $route,
              'levels' => \DB::table('levels')->get()
            ]);
          }else{
            $route = 'pupil_upd';
                $selected_pupil = \DB::table('pupils')->where(['id_user' => \Auth::id()])->get();
//                echo 'isset<br>';
 //               print_r($selected_pupil);
                if($request->ajax()) {
                  $selected_pupil = \DB::table('pupils')->where(['id_user' => \Auth::id(), 'id' => $_POST['sel_id']])->get();
//                  return $request;
                  return ['selected_pupil' => $selected_pupil[0]];
                }else{
                  if(null!=session('sel_id')){
//                    print '!!!!!!'.session('sel_id');
                    $selected_pupil = \DB::table('pupils')->where(['id_user' => \Auth::id(), 'id' => session('sel_id')])->get();
                    session()->forget('sel_id');
                    $selected_pupil = $selected_pupil[0];
                  }else{
                    $selected_pupil = $pupils_data[0];
                  }
                  $new_view = view('pupils', [
                    'select_values' => $this->select_values,
                    'route' => $route,
                    'pupils_data' => $pupils_data,
                    'selected_pupil' => $selected_pupil,
                    'levels' => \DB::table('levels')->get()
                    ]);  
                  return $new_view;
                }
            }
        }else{
            $route = 'pupil_ins';
            return view('new_trainee', [
              'select_values' => $this->select_values,
              'route' => $route,
              'levels' => \DB::table('levels')->get(),
              'fresh' => true
              ]);
         }
      }

      public function delete_p(Request $request){
        $route = 'pupil_upd';
        $pupils_data = \DB::table('pupils')->where('id_user', \Auth::id())->get();
        $selected_pupil = $pupils_data[0];
        $new_view = view('pupils', [
          'select_values' => $this->select_values,
          'route' => $route,
          'pupils_data' => $pupils_data,
          'selected_pupil' => $selected_pupil,
          'levels' => \DB::table('levels')->get()
          ]);  
        return $new_view;
      }

      public function insert_info_p(Request $request){
        $user_upd = new Pupil();
        $this->validate($request, [
          'first_name' => 'min:2',
          'inline_skating' => 'required_without_all:crossminton,skateboarding,crousing',
          'crossminton' => 'required_without_all:inline_skating,skateboarding,crousing',
          'skateboarding' => 'required_without_all:inline_skating,crossminton,crousing',
          'crousing' => 'required_without_all:inline_skating,crossminton,skateboarding'
        ], ['required_without_all' => 'At least one sport must be selected.']);
        $user_upd->insert([
          'id_user' => \Auth::id(),
          'id_gender' => $request->id_gender,
          'first_name' => $request->first_name,
          'last_name' => $request->last_name,
          'birthday' => $request->years.$request->months.$request->days,
          'sport' => ($request->inline_skating=='on'?1:0).', '.($request->crossminton=='on'?1:0).', '.($request->skateboarding=='on'?1:0).', '.($request->crousing=='on'?1:0),
          'level' => ($request->inline_skating_level==''?0:$request->inline_skating_level).', '.($request->crossminton_level==''?0:$request->crossminton_level).', '.($request->skateboarding_level==''?0:$request->skateboarding_level).', '.($request->crousing_level==''?0:$request->crousing_level),
          'weight' => $request->weight,
          'height' => $request->height,
          'foot_size' => $request->foot_size
        ]);
        session(['sel_id' => \DB::getPdo()->lastInsertId()]);
        return redirect()->route('pupils')->with('success','New Trainee was successfully registered');
    }
    
    public function change_info_p(Request $request){
      $user_upd = new Pupil();
      session(['sel_id' => [$request->id]]);
      $this->validate($request, [
        'first_name' => 'min:2',
        'inline_skating' => 'required_without_all:crossminton,skateboarding,crousing',
        'crossminton' => 'required_without_all:inline_skating,skateboarding,crousing',
        'skateboarding' => 'required_without_all:inline_skating,crossminton,crousing',
        'crousing' => 'required_without_all:inline_skating,crossminton,skateboarding'
      ], ['required_without_all' => 'At least one sport must be selected.']);
      $user_upd->where(['id_user' => \Auth::id(), 'id' => $request->id])->update([
        'id_gender' => $request->id_gender,
        'first_name' => $request->first_name,
        'last_name' => $request->last_name,
        'birthday' => $request->years.$request->months.$request->days,
        'sport' => ($request->inline_skating=='on'?1:0).', '.($request->crossminton=='on'?1:0).', '.($request->skateboarding=='on'?1:0).', '.($request->crousing=='on'?1:0),
        'level' => ($request->inline_skating_level==''?0:$request->inline_skating_level).', '.($request->crossminton_level==''?0:$request->crossminton_level).', '.($request->skateboarding_level==''?0:$request->skateboarding_level).', '.($request->crousing_level==''?0:$request->crousing_level),
        'weight' => $request->weight,
        'height' => $request->height,
        'foot_size' => $request->foot_size
      ]);
      return back()->with('success','Information was successfully updated')->with('sel_id',[$request->id]);
    }
}

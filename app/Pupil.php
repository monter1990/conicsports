<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pupil extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
protected $fillable = [
    'id_gender', 'first_name', 'last_name', 'birthday', 'sport', 'level', 'weight', 'height', 'foot_size',
    ];
}

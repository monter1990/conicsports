<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
protected $fillable = [
    'id_user', 'location', 'pupil', 'sport_category', 'sports_subcat', 'level', 'time_frame', 'week_day', 'option_number',
    ];
}
